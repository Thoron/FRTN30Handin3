%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%  Hand-in 3, FRTN30 by Fabian Aagren         %%%%
%%%%  This script requires MATLAB 2015b or later %%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

clear all
clc
close all

%Load everything
load -ASCII traffic.mat
load -ASCII capacities.mat
load -ASCII traveltime.mat
load -ASCII flow.mat

%%
%Set up links
s = [1 2 3 4 1 6 7 8 9  2 3 3 4 5  6  10 10  7  8  9 11 12 13 11 13 14 15 16];
t = [2 3 4 5 6 7 8 9 13 7 8 9 9 14 10 11 15 10 11 12 12 13 14 15 17 17 16 17];
%%
%Create graph and plot with travel times
G = digraph(s, t, traveltime);
plot(G,'Layout','force','EdgeLabel',G.Edges.Weight)

%%
%%%%%%%%%%%%
%%%  a)  %%%
%%%%%%%%%%%%
%Find shortest path from node 1 to 17
sp = shortestpath(G, 1, 17);

disp('The shortest path from 1 to 17 is through nodes ')
disp([num2str(sp) '.']);
disp(' ')
%%
%%%%%%%%%%%%
%%%  b)  %%%
%%%%%%%%%%%%

%Plot graph with maximum flow capacities 
G2 = digraph(s, t, capacities);
plot(G2,'Layout','force','EdgeLabel',G2.Edges.Weight)

%Calculate maxflow
mf = maxflow(G2, 1, 17);

disp(['The maximum flow from 1 to 17 is ' num2str(mf) '.'])
disp(' ')
%%
%%%%%%%%%%%%
%%%  c)  %%%
%%%%%%%%%%%%

%Calculate external in or outflow from every node.
exflows = traffic*flow;

%Plot graph with flows from flow.mat
G3 = digraph(s, t, flow);
plot(G3,'Layout','force','EdgeLabel',G3.Edges.Weight)

disp('The external in or outflow from node 1 through 17 ')
disp([num2str(exflows') '.']);
disp(' ')
%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Preparations for assignments d) - g)  %
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

%%%%%Set up parameters

M = 28;

%Set all net inflow to node 1 and all net outflow to node 17
lambda = zeros(17, 1);
mu = lambda;
lambda(1) = exflows(1);
mu(end) = lambda(1);
%%
%%%%%%%%%%%%
%%%  d)  %%%
%%%%%%%%%%%%

%Solve social optimum
cvx_begin
    variable f(M)
    minimize sum(traveltime.*capacities .* inv_pos(1-f./capacities) - traveltime.*capacities)
    subject to
        traffic*f == lambda - mu
        0 <= f <= capacities
cvx_end
%%
%Plot social optimum
G5 = digraph(s,t,f);
figure
plot(G5,'Layout','force','EdgeLabel',round(G5.Edges.Weight))

%%
%%%%%%%%%%%%
%%%  e)  %%%
%%%%%%%%%%%%

%Solve the Wardrop equilibrium
cvx_begin
    variable g(M)
    minimize sum(-traveltime.*capacities .* log(1 - g./capacities))
    subject to
        traffic*g == lambda - mu
        0 <= g <= capacities
cvx_end

%%
%Plot Wardrop equilibrium
G6 = digraph(s,t,g);
figure
plot(G6,'Layout','force','EdgeLabel',round(G6.Edges.Weight))

%%
%%%%%%%%%%%%
%%%  f)  %%%
%%%%%%%%%%%%

%Set up tolls
w_e = f.*(traveltime.*capacities./((capacities - f).^2));

%Solve the new Wardrop equilibrium
cvx_begin
    variable h(M)
    minimize sum(-traveltime.*capacities .* log(1 - h./capacities) + w_e.*h)
    subject to
        traffic*h == lambda - mu
        0 <= h <= capacities
cvx_end

%%
%Plot the new Wardrop equilibrium
G7 = digraph(s,t,h);
figure
plot(G7,'Layout','force','EdgeLabel', round(G7.Edges.Weight))

%%
%%%%%%%%%%%%
%%%  g)  %%%
%%%%%%%%%%%%

%Compute system optimum
cvx_begin
	variable p(M)
	p1 = 0;
	for k = 1:M
		p1 = p1 + traveltime(k)*quad_over_lin(p(k),capacities(k) - p(k));
    end
	minimize p1
	subject to
        traffic*p == lambda - mu
        0 <= p <= capacities
cvx_end	
%%
%Plot system optimum
G8 = digraph(s,t,p);
figure
plot(G8,'Layout','force','EdgeLabel', round(G8.Edges.Weight))
%%

%Construct tolls to get a Wardrob equilibrium equal to the system optimum
w_e = p.*(traveltime.*capacities./((capacities - p).^2)) - traveltime;

%Solve the system
cvx_begin
    variable q(M)
    minimize sum(-traveltime.*capacities .* log(1 - q./capacities) + w_e.*q)
    subject to
        traffic*q == lambda - mu
        0 <= q <= capacities
cvx_end
%%
%Plot the Wardrob equilibrium
G9 = digraph(s,t,q);
figure
plot(G9,'Layout','force','EdgeLabel', round(G9.Edges.Weight))

%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%                      %%%%%%%
%%%%%%%%   PART 2 - Coloring  %%%%%%%
%%%%%%%%                      %%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

%clear all
clear all
clc
close all

%%
%%%%%%%%%%%%
%%%  a)  %%%
%%%%%%%%%%%%

%Construct the line graph 
G = graph(1:9, 2:10);

%Set up the state vector
% 0 is red
% 1 is green
X = zeros(10,1);

%Number of iterations
itermax = 50;

%Animation speed in seconds
speed = .1;

%Iterate and update states
for t = 1 : itermax
    %Animate
    p = plot(G);
    highlight(p, find(X == 1),'NodeColor','g')
    highlight(p, find(X == 0),'NodeColor','r')
    %Slow down animation
    pause(speed)
    %Select a node at random
    I = randi(10);
    %Get neighbours
    neigh = neighbors(G, I);
    %Pre-calculate sums
    fsum = sum(X(neigh) == 1);
    lsum = sum(X(neigh) == 0);
    %Calculate probability of getting state green
    P = exp(-t/100*fsum)/(exp(-t/100*fsum) + exp(-t/100*lsum));
    %Randomly select a new state and update state vector
    X(I) = rand < P;
end

%Calculate the potential
isum = zeros(10, 1);
for i = 1 : 10
    isum(i) = sum(X(i) == X(neighbors(G, i)));
end
U = .5*sum(isum);

disp(['The potential is ' num2str(U)])
disp(' ')

%%
%Clear is necessary 
clear all
clc
close all

%Load wifi data
load -ASCII wifi.mat

%Construct graph
G = graph(wifi);

%Set up the state vector
X = ones(100, 1);

%Number of iterations
itermax = 1000;

%Set the beta function
beta = (1 : itermax)/100;
%beta = sqrt(1 : itermax);

%Iterate
for t = 1 : itermax
    %Randomly select a node
    I = randi(100);
    %Extract its neighbors
    neigh = neighbors(G, I);
    %Pre-calculate sums
    for j = 1 : 8
        lsum(j) = sum(c(X(neigh), j));
    end
    %Calculate probability
    P = exp(-beta(t)*lsum)/sum(exp(-beta(t)*lsum));
    %Randomly select a new state for node I
    X(I) = find(cumsum(P) > rand, 1);
end

%Calculate the potential
isum = zeros(100, 1);
for i = 1 : 100
    isum(i) = sum(c(X(i), X(neighbors(G, i))));
end
U = .5*sum(isum);

%Display the result
disp(['The potential is ' num2str(U)])
disp(' ')

%Plot the assigned channels
p = plot(G);
highlight(p, find(X == 1),'NodeColor','r')
highlight(p, find(X == 2),'NodeColor','g')
highlight(p, find(X == 3),'NodeColor','b')
highlight(p, find(X == 4),'NodeColor','y')
highlight(p, find(X == 5),'NodeColor','m')
highlight(p, find(X == 6),'NodeColor','c')
highlight(p, find(X == 7),'NodeColor','w')
highlight(p, find(X == 8),'NodeColor','k')

