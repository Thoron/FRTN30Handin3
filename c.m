%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Cost function for assignment 2 b) Hand-in 3.
% X is a vector of values that is to be compared with scalar s
% v is a vector of same size as X with the answers 0, 1 or 2
% By Fabian Aagren, FRTN30
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

function v = c(X, s)
    diff = abs(X-s);
    v = zeros(size(diff));
    v(diff == 0) = 2;
    v(diff == 1) = 1;
end